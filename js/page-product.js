'use strict';

const form = document.querySelector('form');
const inputName = document.querySelector('.feedback__name');
const inputNameError = document.querySelector('.feedback__error-name');
const inputRating = document.querySelector('.feedback__rating');
const inputRatingError = document.querySelector('.feedback__error-rating');
const inputs = form.querySelectorAll('input, textarea');

const countCart = document.querySelector('.header__icon-cart-count');
const countFavorite = document.querySelector('.header__icon-favorite-count');
const productId = document.querySelector('[name="product-id"]').value;
const addCart = document.querySelector('.block-price__btn');
const addFavorite = document.querySelector('.block-price__like-ico');

// Начало слайдера
let swiper = new Swiper(".mySwiper", {
  loop: true,
  spaceBetween: 20,
  slidesPerView: 'auto',
  loopedSlides: 3,
  breakpoints: {
    320: {
      spaceBetween: 5
    }
  }
});
// Конец слайдера

document.addEventListener("DOMContentLoaded", () => {
  countCart.textContent = cart.length;
  if (cart.length === 0) {
    countCart.textContent = '';
    countCart.style.opacity = '0';
  }
  if (cart.length > 0) {
    addCart.textContent = 'Товар уже в корзине';
    addCart.classList.add('incart');
    countCart.style.opacity = '1';
  }

  function addToCart(id) {
    console.log(id);
    if (!cart.includes(id)) {
      cart.push(id);
      localStorage.setItem('goods', cart);
    }
  };

  function removeFromCart(id) {
    let i = cart.indexOf(id);
    if (i !== -1) {
      cart.splice(i, 1);
      localStorage.setItem('goods', cart);
    }
  };

  addCart.addEventListener("click", () => {
    if (cart.length > 0) {
      addCart.textContent = 'Добавить в корзину';
      addCart.style.background = '#F36223';
      removeFromCart(productId);
      countCart.textContent = cart.length
      countCart.style.opacity = '0';
      if (localStorage.getItem('goods') === '') {
        localStorage.removeItem('goods')
      }
      if (cart.length === 0) {
        countCart.textContent = ''
      }
    } else {
      addCart.textContent = 'Товар уже в корзине';
      addCart.classList.add('incart');
      addToCart(productId);
      countCart.textContent = cart.length
      countCart.style.opacity = '1';
    }
  });

  form.addEventListener("submit", (evt) => {
    evt.preventDefault();
    validateName();
    validateRating();
    if (inputNameError.classList.contains('error')) {
      inputRatingError.classList.remove('error');
    }
    if (validateName() && validateRating()) {
      inputs[0].value = '';
      inputs[1].value = '';
      inputs[2].value = '';
      localStorage.clear();
    }
  })
  function validateName() {
    inputName.value = inputName.value.replace(/\s+/g, ' ').trim()
    inputName.classList.add('error');
    inputNameError.classList.add('error')
    if (inputName.value.length === 0) {
      inputNameError.textContent = 'Вы забыли указать имя и фамилию';
    } else if (inputName.value.length < 2) {
      inputNameError.textContent = 'Имя не может быть короче 2-хсимволов';
    } else {
      removeError();
      return true;
    }
    inputName.addEventListener("input", removeError);
  }
  
  function validateRating() {
    if (inputRating.value >= 1 && inputRating.value <= 5) {
      inputRating.classList.remove('error');
      inputRatingError.classList.remove('error');
      return true;
    } else {
      inputRating.classList.add('error');
      inputRatingError.classList.add('error');
      inputRatingError.textContent = 'Оценка должна быть от 1 до 5';
    }
    inputRating.addEventListener("input", () => {
      inputRating.classList.remove('error');
      inputRatingError.classList.remove('error')
    });
  }
  
  let removeError = () => {
    inputName.classList.remove('error');
    inputNameError.classList.remove('error')
  }

  inputs.forEach(function (input) {
    input.value = localStorage.getItem(input.name)
    input.addEventListener('input', () => {
      localStorage.setItem(input.name, input.value)
    })
  })
});

let cart = [];
if (localStorage.getItem('goods') > 0) {
  cart = (localStorage.getItem('goods')).split(',');
};

inputs.forEach(function (input) {
  input.value = localStorage.getItem(input.name)
  input.addEventListener('input', () => {
    localStorage.setItem(input.name, input.value)
  })
})

