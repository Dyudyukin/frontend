'use strict';

const fullFormNode = document.querySelector('form');
const inputNameNode = document.querySelector('.feedback__name');
const inputRatingNode = document.querySelector('.feedback__rating');
const inputNameError = document.querySelector('.feedback__error-name');
const inputRatingError = document.querySelector('.feedback__error-rating');
const inputs = fullFormNode.querySelectorAll('input, textarea');

class Form {
  constructor(formNode, rules) {
    this._formNode = formNode;
    this._rules = rules;

    this.init = this.init.bind(this);
    this.validate = this.validate.bind(this);
  }

  init() {
    this._formNode.addEventListener('submit', (evt) => {
      evt.preventDefault();

      for (let rule of this._rules) {
        this.validate(rule);
      }
    })
  }

  validate(rule) {
    const {condition, node, nodeError, errorText} = rule;
    if (!condition) {
      console.log(inputNameNode.value.length);
      node.classList.add('error')
      nodeError.classList.add('error')
      nodeError.textContent = errorText;
    } else {
      nodeError.classList.remove('error')
      node.classList.remove('error')
    }
  }
}

let rules = [
  {
    condition: this.value.length === 0,
    node: inputNameNode,
    nodeError: inputNameError,
    errorText: 'Вы забыли указать имя и фамилию',
  },
  {
    condition: this.length < 2,
    node: inputNameNode,
    nodeError: inputNameError,
    errorText: 'Имя не может быть короче 2-х символов',
  },
  {
    condition: this.value >= 1 && this.value <= 5,
    node: inputRatingNode,
    nodeError: inputRatingError,
    errorText: 'Оценка должна быть от 1 до 5',
  },
];

const form = new Form(fullFormNode, rules);
form.init();
