'use strict';
window.addEventListener('load', (event) => {

const form = document.querySelector('form');
const inputName = document.querySelector('.feedback__name');
const inputNameError = document.querySelector('.feedback__error-name');
const inputRating = document.querySelector('.feedback__rating');
const inputRatingError = document.querySelector('.feedback__error-rating');
const inputs = form.querySelectorAll('input, textarea');

class Form {
  constructor(input) {
    this.input = input;
    this.value = this.input.replace(/\s+/g, ' ').trim();
    this.length = this.value.length;
  }
  get removeErrorName() {
    inputName.classList.remove('error');
    inputNameError.classList.remove('error')
  }
  get removeErrorRating() {
    inputRating.classList.remove('error');
    inputRatingError.classList.remove('error')
  }
  get addErrorName() {
    inputName.classList.add('error');
    inputNameError.classList.add('error');
  }
  get addErrorRating() {
    inputRating.classList.add('error');
    inputRatingError.classList.add('error');
  }
}

class AddReviewForm extends Form {
  constructor(input) {
    super(input)
  }

  get validateRating() {
    if (this.value >= 1 && this.value <= 5) {
      this.removeErrorRating;
      return true;
    } else {
      this.addErrorRating;
      inputRatingError.textContent = 'Оценка должна быть от 1 до 5';
    }
    inputRating.addEventListener("input", function () {
      inputRating.classList.remove('error');
      inputRatingError.classList.remove('error')
    });
  }

  get validateName() {
    this.addErrorName
    if (this.length === 0) {
      inputNameError.textContent = 'Вы забыли указать имя и фамилию';
    } else if (this.length < 2) {
      inputNameError.textContent = 'Имя не может быть короче 2-хсимволов';
    } else {
      this.removeErrorName;
      return true;
    }
    inputName.addEventListener("input", function () {
      inputName.classList.remove('error');
      inputNameError.classList.remove('error')
    });
  }
}

form.addEventListener("click", function (evt) {
  evt.preventDefault();
  let valName = new AddReviewForm(inputName.value);
  let valRating = new AddReviewForm(inputRating.value);
  valName.validateName
  valRating.validateRating
  if (inputNameError.classList.contains('error')) {
    inputRatingError.classList.remove('error');
  }
  if (valName.validateName && valRating.validateRating) {
    inputs[0].value = '';
    inputs[1].value = '';
    inputs[2].value = '';
    localStorage.clear();
  }
})

inputs.forEach(function (input) {
  input.value = localStorage.getItem(input.name)
  input.addEventListener('input', function () {
    localStorage.setItem(input.name, input.value)
  })
})
});