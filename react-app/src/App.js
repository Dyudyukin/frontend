import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import PageProduct from './components/PageProduct/PageProduct';
import HomePage from './components/HomePage/HomePage';
import './App.css';

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path='/product' element={<PageProduct />} />
        <Route path='/' element={<HomePage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
