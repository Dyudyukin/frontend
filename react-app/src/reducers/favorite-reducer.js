import { createSlice } from '@reduxjs/toolkit';

const getFavOnLocalStorage = JSON.parse(localStorage.getItem('Favorites'));

export const favoriteSlice = createSlice({

  name: 'favorite',

  initialState: {
    likes: getFavOnLocalStorage ? [getFavOnLocalStorage] : [],
  },

  reducers: {

    addLike: (prevState, action) => {
      const like = action.payload

      return {

        ...prevState,
        likes: [...prevState.likes, like],

      };
    },

    removeLike: (prevState, action) => {
      const like = action.payload;

      return {
        ...prevState,
        likes: prevState.likes.filter((prevLike) => {
          return prevLike.id !== like.id;
        })
      };
    },

  },
});

export const { addLike, removeLike } = favoriteSlice.actions;
export default favoriteSlice.reducer;
