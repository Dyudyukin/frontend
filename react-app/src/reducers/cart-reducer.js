import { createSlice } from '@reduxjs/toolkit';

const getCartOnLocalStorage = JSON.parse(localStorage.getItem('Cart'));

export const cartSlice = createSlice({
  
  name: 'cart',

  initialState: {
    products: getCartOnLocalStorage ? [getCartOnLocalStorage] : [],
  },

  reducers: {

    addProduct: (prevState, action) => {
      const product = action.payload

      return {

        ...prevState,
        products: [...prevState.products, product],

      };
    },

    removeProduct: (prevState, action) => {
      const product = action.payload;

      return {
        ...prevState,
        products: prevState.products.filter((prevProduct) => {
          return prevProduct.id !== product.id;
        })
      };
    },

  },
});

export const { addProduct, removeProduct } = cartSlice.actions;
export default cartSlice.reducer;
