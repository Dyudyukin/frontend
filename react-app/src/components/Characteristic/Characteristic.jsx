import React from 'react';
import productInfo from '../productInfo';

function Characteristic(props) {

  return (
    <section className="characteristic">
      <h3 className="characteristic__name">Характеристики товара</h3>
      <ul className="characteristic__info">
        <li>Экран: <b>{productInfo.characteristics.display}</b></li>
        <li>Встроенная память: <b>{productInfo.characteristics.currentMemory}</b></li>
        <li>Операционная система: <b>{productInfo.characteristics.operatingSystem}</b></li>
        <li>Беспроводные интерфейсы: <b>{productInfo.characteristics.wirelessInterfaces}</b></li>
        <li>Процессор: <a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank" rel="noreferrer"><b>{productInfo.characteristics.processor}</b></a></li>
        <li>Вес: <b>{productInfo.characteristics.weight}</b></li>
      </ul>
    </section>
  )
}

export default Characteristic;
