import React from 'react';

import Header from '../Header/Header'
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import Gallery from '../Gallery/Gallery';
import Config from '../Config/Config';
import Colors from '../Colors/Colors'
import Characteristic from '../Characteristic/Characteristic';
import Description from '../Description/Description';
import Compare from '../Compare/Compare';
import Sidebar from '../Sidebar/Sidebar';
import Rewiews from '../Reviews/Reviews';
import Form from '../Form/Form';
import Footer from '../Footer/Footer'

import './PageProduct.css';
import './normalize.css';

function PageProduct(props) {

  return (<div>

    <Header />
    <main className="main wrapper">
      <Breadcrumbs />
      <Gallery />
      <div className="center-wrap">
        <div className="center-wrap__left">
          <Colors />
          <Config />
          <Characteristic />
          <Description />
          <Compare />
          <Rewiews />
          <Form />
        </div>
        <Sidebar />
      </div>
    </main>
    <Footer />

  </div>);
}

export default PageProduct;