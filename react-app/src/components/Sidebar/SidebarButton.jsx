import React from 'react';

function SidebarButton(props) {

  const { handleSubmit, hasInCart, item } = props;

  return (
    <div className="block-price__button">
      <button className={!hasInCart ? `block-price__btn` : `block-price__btn-incart`} aria-label="Добавить в корзину" type="submit"
        onClick={(e) => { handleSubmit(item) }}>{!hasInCart ? `Добавить в корзину` : `Товар уже в корзине`}</button>
    </div>
  )
}

export default SidebarButton;