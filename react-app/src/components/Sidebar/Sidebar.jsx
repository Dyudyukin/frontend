import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addProduct, removeProduct } from '../../reducers/cart-reducer';
import { addLike, removeLike } from '../../reducers/favorite-reducer';
import Iframe from '../Iframe/Iframe';
import SidebarButton from './SidebarButton';

function Sidebar(props) {

  // const [addFavorite, setAddFavorite] = useState(localStorage.getItem('Favorites') || '');
  const dispatch = useDispatch();
  const products = useSelector((state) => state.cart.products)
  const likes = useSelector((state) => state.favorite.likes)
  const item = { id: 1234 }
  const hasInCart = products.some((prevProduct) => prevProduct.id === item.id);
  const hasInFavorite = likes.some((prevLike) => prevLike.id === item.id);

  const handleClick = () => {
    if (hasInFavorite) {
      const action = removeLike(item);
      dispatch(action);
      localStorage.removeItem('Favorites')
    } else {
      const action = addLike(item);
      dispatch(action);
      localStorage.setItem('Favorites', JSON.stringify(item))
    } return hasInFavorite
  }

  const handleSubmit = (item) => {

    if (hasInCart) {
      const action = removeProduct(item);
      dispatch(action);
      localStorage.removeItem('Cart');
    } else {
      const action = addProduct(item);
      dispatch(action);
      localStorage.setItem('Cart', JSON.stringify(item));
    } return hasInCart
  }

  return (<aside className="center-wrap__right">
    <div className="block-price">
      <div className="block-price__item">
        <div className="block-price__cost">
          <span className="block-price__old">75 990</span>
          <span className="block-price__sell">8%</span>
          <span className={!hasInFavorite ? `block-price__like` : `block-price__like-checked`}>
            <input className="block-price__like-ico" id="favorite" type="checkbox" name="add-favorite" value="1" onClick={handleClick} />
            <label htmlFor="favorite">
              <svg width="44px" height="35px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M17.19 4.156c-1.672-1.535-4.383-1.535-6.055 0L10 5.197 8.864 4.156c-1.672-1.535-4.382-1.535-6.054 0-1.881 1.726-1.881 4.519 0 6.245L10 17l7.19-6.599c1.88-1.726 1.88-4.52 0-6.245zm-1.066 5.219L10 15.09 3.875 9.375c-.617-.567-.856-1.307-.856-2.094s.138-1.433.756-1.999c.545-.501 1.278-.777 2.063-.777.784 0 1.517.476 2.062.978L10 7.308l2.099-1.826c.546-.502 1.278-.978 2.063-.978s1.518.276 2.063.777c.618.566.755 1.212.755 1.999s-.238 1.528-.856 2.095z" /></svg>
            </label>
          </span>
        </div>
        <span className="block-price__real-cost">67 990</span>
      </div>
      <div className="block-price__item">
        <span className="block-price__delivery">Самовывоз в четверг, 1 сентября — <b>бесплатно</b></span>
        <span className="block-price__delivery">Курьером в четверг, 1 сентября — <b>бесплатно</b></span>
      </div>
      <SidebarButton hasInCart={hasInCart} item={item} handleSubmit={handleSubmit} />
    </div>

    <Iframe />

  </aside>);
}

export default Sidebar;