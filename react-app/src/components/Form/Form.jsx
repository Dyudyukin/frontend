import React, { useState } from 'react';
import './Form.css';

function Form() {

  const [name, setName] = useState(localStorage.getItem('Имя') || '');
  const [rating, setRating] = useState(localStorage.getItem('Рейтинг') || '');
  const [comment, setComment] = useState(localStorage.getItem('Комментарий') || '');

  const [errorName, setErrorName] = useState('');
  const [errorRating, setErrorRating] = useState('');


  let goodName = false;
  let goodRating = false;

  const handleSubmit = (evt) => {
    evt.preventDefault();
    validateName();
    validateRating();
    clearForm();
  }

  const validateRating = () => {
    if (rating >= 1 && rating <= 5) {
      goodName = true;
    } else {
      setErrorRating('Оценка должна быть от 1 до 5');
    }
  }

  const validateName = () => {
    let valName = name.replace(/\s+/g, ' ').trim()
    if (valName.length === 0 || valName === '') {
      setErrorName('Вы забыли указать имя и фамилию')
    }
    if (valName.length < 2) {
      setErrorName('Имя не может быть короче 2х-символов')
    } else {
      goodRating = true;
    }
  }

  const clearForm = () => {
    if (goodName === false && goodRating === false) {
      setErrorRating('')
    }
    if (goodName === true && goodRating === true) {
      setName('')
      setRating('')
      setComment('')
      localStorage.removeItem('Имя')
      localStorage.removeItem('Рейтинг')
      localStorage.removeItem('Комментарий')
      alert('Ваш отзыв был успешно отправлен и будет отображён после модерации');
    }
  }

  return (<section className="feedback">
    <form className="feedback-form" action="https://echo.htmlacademy.ru/" method="POST" onSubmit={handleSubmit}>
      <div className="feedback__list feedback-margin-left">
        <h3>Добавить свой отзыв</h3>
        <div className="feedback__contact">
          <div className="feedback__fields">
            <input className={`${`feedback__name`} ${errorName && `error`}`} type="text" name="name" id="name" value={name} placeholder="Имя и фамилия"
              onChange={(evt) => { setName(evt.target.value); setErrorName(''); localStorage.setItem('Имя', evt.target.value) }}
            />
            <input className={`${`feedback__rating`} ${errorRating && `error`}`} type="number" name="rating" id="rating" value={rating} placeholder="Оценка"
              onChange={(evt) => { setRating(evt.target.value); setErrorRating(''); localStorage.setItem('Рейтинг', evt.target.value) }}
            />
          </div>
          <div className="feedback__error">
            <div className={errorName ? `feedback__error-name` : `name-invisible`}>{errorName}</div>
            <div className={errorRating ? `feedback__error-rating` : `rating-invisible`}>{errorRating}</div>
          </div>
        </div>
        <div className="feedback__text">
          <textarea className="feedback__textarea" name="feedback" value={comment} placeholder="Текст отзыва"
            onChange={(evt) => { setComment(evt.target.value); localStorage.setItem('Комментарий', evt.target.value) }}>
          </textarea>
        </div>
        <button className="feedback__button" aria-label="Отправить отзыв" type="submit">Отправить отзыв</button>
      </div>
    </form>
  </section>);
}

export default Form;