import React from "react";

function GalleryItem(props) {

  const { gallery } = props;

  return (
    <div className="swiper-slide"><img src={gallery.img} alt={gallery.alt} /></div>
  )

}

export default GalleryItem;