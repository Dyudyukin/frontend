import React from "react";

import GalleryItem from "./GalleryItem";
import galleryImg from "../galleryImg";

function Gallery() {

  return (
    <section className="gallery">
      <h2 className="gallery__name">Смартфон Apple iPhone 13, синий</h2>
      <div className="swiper mySwiper">
        <div className="swiper-wrapper">
          {galleryImg.map((gallery) => (
            <GalleryItem key={gallery.id} gallery={gallery} />
          )
          )}
        </div>
      </div>
    </section>
  )

}

export default Gallery;