const memorys = [
  { id: 'configuration-128', value: '128', title: '128 ГБ' },
  { id: 'configuration-256', value: '256', title: '256 ГБ' },
  { id: 'configuration-512', value: '512', title: '512 ГБ' },
]

export default memorys;