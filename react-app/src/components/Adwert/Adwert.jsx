import React from 'react';
import './Adwert.css';

function Adwert(props) {
  return (<div className="ads">
  <div className="ads__wrapper">
    <a href="https://stc.innopolis.university/junior-frontend" target="_blank" rel="noreferrer">
      <img src="./img/innopolis.png" alt="" />
    </a>
  </div>
</div>);
}

export default Adwert;