import React from 'react';

function ConfigButton(props) {

  const { memory, size, setSize } = props;

  return (
    <li className="configuration__btn">
      <input id={memory.id} type="radio" name="configuration-btn" value={memory.value}
        onClick={e => {
          setSize(memory.title)
          localStorage.setItem('Memory', memory.title);
        }}
        defaultChecked={memory.title === size} />
      <label htmlFor={memory.id}>
        <span className="configuration__btn-item">{memory.title}</span>
      </label>
    </li>
  )
}

export default ConfigButton;