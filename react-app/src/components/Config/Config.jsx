import React, { useState } from 'react';
import productInfo from '../productInfo';
import ConfigButton from './ConfigButton';

function Config() {

  const [size, setSize] = useState(localStorage.getItem('Memory') || '128 ГБ')

  return (<section className="configuration">
    <h3 className="configuration__name">Конфигурация памяти: {size}</h3>
    <ul className="configuration__list">
      {productInfo.memorys.map((memory) => (
        <ConfigButton key={memory.id} memory={memory} size={size} setSize={setSize} />
      )
      )}
    </ul>
  </section>);
}

export default Config;