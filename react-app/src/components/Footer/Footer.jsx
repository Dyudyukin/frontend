import React from 'react';
import { useCurrentDate } from "@kundinos/react-hooks";

import styled from 'styled-components';
// import './Footer.css';

const FooterWrapper = styled.footer`
    width: 100%;
    background-color: #f2f2f2;
    margin: 0 auto;
    transition: all 0.5s ease-out;
  `;

const FooterList = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    max-width: 1500px;
    padding: 0 50px;
    margin: 0 auto;
    transition: all 0.5s ease-out;

    @media (max-width: 1499px) {
      max-width: 1024px;
      padding: 0 15px;
    }

    @media (max-width: 790px) {
      max-width: 360px;
      padding: 0 10px;
      flex-direction: column;
      align-items: flex-start;
    }
  `;

const FooterCopyright = styled.div`
    font-size: 14px;
    padding: 30px 0;
  `;

const FooterText = styled.p`
    margin: 0;
    line-height: 17px;
  `;
const ColorText = styled.span`
    color: #f52;
  `;

const FooterUp = styled.span`
    margin-bottom: 0px;

    @media (max-width: 790px) {
      margin-bottom: 20px;
    }
  `;

function Footer(props) {

  const currentDate = useCurrentDate({ every: "day" });
  const fullYear = currentDate.getFullYear();

  return (<FooterWrapper>
    <FooterList>
      <FooterCopyright>
        <FooterText><b>&copy; ООО «<ColorText>Мой</ColorText>Маркет», 2018-{fullYear}</b></FooterText>
        <FooterText>Для уточнения информации звоните по номеру <a href="tel:+79000000000">+7&nbsp;(900)&nbsp;00-00</a>,</FooterText>
        <FooterText>а предложения по сотрудничеству отправляйте на почту <a href="mailto:partner@mymarket.com">partner@mymarket.com</a></FooterText>
      </FooterCopyright>
      < FooterUp>
        <a href="#top">Наверх</a>
      </ FooterUp>
    </FooterList>
  </FooterWrapper>);

}

export default Footer;