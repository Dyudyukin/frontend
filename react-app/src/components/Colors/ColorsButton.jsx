import React from 'react';

function ColorsButton(props) {

  const { data, saveColor, setSaveColor } = props;


  return (
    <li className="color-choice__btn">
      <input id={data.id} type="radio" name="choice-color" value={data.id}
        onChange={e => {
          setSaveColor(data.value)
          localStorage.setItem('Color', data.value);
        }}
        defaultChecked={data.value === saveColor} />
      <label htmlFor={data.id}>
        <img className="color-choice__img" src={data.image} alt={data.alt} />
      </label>
    </li>
  )
}

export default ColorsButton;