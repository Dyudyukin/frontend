import React, { useState } from 'react';
import productInfo from '../productInfo';
import ColorsButton from './ColorsButton';

function Colors(props) {

  const [saveColor, setSaveColor] = useState(localStorage.getItem('Color') || 'чёрный')

  return (
    <section className="color-choice">
      <h3 className="color-choice__name">Цвет товара: {saveColor}</h3>
      <ul className="color-choice__list">
        {productInfo.colors.map((color) => (
          <ColorsButton key={color.id} data={color} saveColor={saveColor} setSaveColor={setSaveColor} />
        )
        )}
      </ul>
    </section>
  )
}

export default Colors;