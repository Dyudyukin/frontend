import React from 'react';
import comments from '../comments';
import Review from './Review';

function Rewiews(props) {

  return (<section className="comments comments_margin-top">
    <div className="comments__header">
      <div className="comments__header-title">
        <h3 className="comments__name">Отзывы</h3>
        <span className="comments__count">425</span>
      </div>
    </div>
    <ul className="comments__list">
      {comments.map((comment) => (
        <Review key={comment.id} user={comment} />
      )
      )}
    </ul>
  </section>
  )
}

export default Rewiews;