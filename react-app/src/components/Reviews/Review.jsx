import React from 'react';

function Review(props) {

  const { user } = props;

  return (
    <li className="comments__item">
      <div className="comments__container">
        <div className="comments__photo">
          <img className="comments__photo-img" src={user.avatar} alt="Фото автора" />
        </div>
        <div className="comments__content content">
          <div className="content__name">
            <h4>{user.name}</h4>
          </div>
          <div className="content__rating">
            <div className="content__rating-list">
              <input type="radio" id={`${user.id}-star5`} name={`${user.id}-rating`} value="5" defaultChecked={'5' === user.rating} disabled />
              <label htmlFor={`${user.id}-star5`}>5 stars</label>
              <input type="radio" id={`${user.id}-star4`} name={`${user.id}-rating`} value="4" defaultChecked={'4' === user.rating} disabled />
              <label htmlFor={`${user.id}-star4`}>4 stars</label>
              <input type="radio" id={`${user.id}-star3`} name={`${user.id}-rating`} value="3" defaultChecked={'3' === user.rating} disabled />
              <label htmlFor={`${user.id}-star3`}>3 stars</label>
              <input type="radio" id={`${user.id}-star2`} name={`${user.id}-rating`} value="2" defaultChecked={'2' === user.rating} disabled />
              <label htmlFor={`${user.id}-star2`}>2 stars</label>
              <input type="radio" id={`${user.id}-star1`} name={`${user.id}-rating`} value="1" defaultChecked={'1' === user.rating} disabled />
              <label htmlFor={`${user.id}-star1`}>1 star</label>
            </div>
          </div>
          <div className="content__experience">
            <p><b>Опыт использования:</b>{user.content.experience}</p>
          </div>
          <div className="content__quality">
            <p><b>Достоинства:</b></p>
            <p>{user.content.quality}</p>
          </div>
          <div className="content__defect">
            <p><b>Недостатки:</b></p>
            <p>{user.content.defect}</p>
          </div>
        </div>
      </div>
    </li>

  )
}

export default Review;

