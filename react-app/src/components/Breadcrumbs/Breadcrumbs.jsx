import React from 'react';
import links from '../links';

function Breadcrumbs(props) {
  return (
    <nav className="breadcrumbs">
      <ul className="breadcrumbs__list">
        {links.map((items) => (
          <li className="breadcrumbs__item" key={items.id}>
            <a className="breadcrumbs__link" href={items.link}>{items.name}</a>
            <span className="breadcrumbs__separator">&gt;</span>
          </li>
        )
        )}
      </ul>
    </nav>
  )
}

export default Breadcrumbs;

