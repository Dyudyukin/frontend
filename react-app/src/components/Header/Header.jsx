import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from "react-redux";
import styles from './Header.module.css';
// import './Header.css';

function Header(props) {

  const cartCount = useSelector((state) => state.cart.products);
  const favoriteCount = useSelector((state) => state.favorite.likes);

  return (<div className={styles.header_wrap}>
    <header className={styles.header}>
      <Link to="/">
        <div className={styles.header__logo}>
          <img className={styles.header__img} src="./img/favicon.svg" alt="Логотип" />
          <h1 className={styles.header__name}>
            <span className={styles.colortext}>Мой</span>Маркет
          </h1>
        </div>
      </Link>
      <div className={styles.header__icon}>
        <a className={styles.header__icon_favorite} href="#!" target="_self">
          <span className={favoriteCount.length > 0 ? styles.header__icon_favorite_count : ''}>{favoriteCount.length > 0 && favoriteCount.length}</span>
          <svg width="100%" height="100%" viewBox="0 0 44 35" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" clipRule="evenodd" d="M3.30841 2.95447C7.29791 -0.875449 13.7444 -0.875449 17.7339 2.95447L22.0002 7.05027L26.2667 2.95447C30.2563 -0.875449 36.7027 -0.875449 40.6923 2.95447C44.6817 6.78439 44.6817 12.973 40.6923 16.803L22.0002 34.7472L3.30841 16.803C-0.681091 12.973 -0.681091 6.78439 3.30841 2.95447ZM14.7876 5.78289C12.4253 3.51507 8.61701 3.51507 6.25468 5.78289C3.89237 8.05071 3.89237 11.7067 6.25468 13.9746L22.0002 29.0904L37.7461 13.9746C40.1084 11.7067 40.1084 8.05071 37.7461 5.78289C35.3838 3.51507 31.5755 3.51507 29.2132 5.78289L22.0002 12.7072L14.7876 5.78289Z" fill="#888888" />
          </svg>
        </a>
        <a className={styles.header__icon_cart} href="#!" target="_self">
          <span className={cartCount.length > 0 ? styles.header__icon_cart_count : ''}>{cartCount.length > 0 && cartCount.length}</span>
          <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 24 24">
            <path d="M6 6a4 4 0 0 1 4-4h4a4 4 0 0 1 4 4v2h4a1 1 0 0 1 .986 1.164l-1.582 9.494A4 4 0 0 1 17.46 22H6.54a4 4 0 0 1-3.945-3.342L1.014 9.164A1 1 0 0 1 2 8h4V6Zm2 2h5a1 1 0 1 1 0 2H3.18l1.389 8.329A2 2 0 0 0 6.54 20h10.92a2 2 0 0 0 1.972-1.671L20.82 10H17a1 1 0 0 1-1-1V6a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v2Z"></path>
          </svg>
        </a>
      </div>
    </header>
  </div>);
}

export default Header;