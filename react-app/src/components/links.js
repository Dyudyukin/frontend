const links = [
  {
    id: '1',
    name: 'Электроника',
    link: '#!',
  },
  {
    id: '2',
    name: 'Смартфоны и гаджеты',
    link: '#!',
  },
  {
    id: '3',
    name: 'Мобильные телефоны',
    link: '#!',
  },
  {
    id: '4',
    name: 'Apple',
    link: '#!',
  },
];

export default links;