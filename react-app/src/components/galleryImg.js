const galleryImg = [
  { id: '1', img: './img/image-1.webp', alt: 'Общий вид телефона, экран на первом плане' },
  { id: '2', img: './img/image-2.webp', alt: 'Экран телефона, вид спереди' },
  { id: '3', img: './img/image-3.webp', alt: 'Общий вид телефона, под углом' },
  { id: '4', img: './img/image-4.webp', alt: 'Крупное фото основной камеры' },
  { id: '5', img: './img/image-5.webp', alt: 'Общий вид телефона, задняя панель на первом плане' },
];

export default galleryImg;