import React from 'react';
import { Link } from "react-router-dom";

import Header from '../Header/Header'
import Footer from '../Footer/Footer'

import './HomePage.css';
import '../PageProduct/normalize.css';

function HomePage(props) {

  return (
    <div className='homepage'>

      <Header />

      <main className="wrapper main__wrap">
          <div className='main__content'>
            <p className='main__text' >Здесь должно быть содержимое главной&nbsp;страницы.
              Но&nbsp;в&nbsp;рамках курса главная страница используется&nbsp;лишь в&nbsp;демонстрационных целях</p><br />
            <Link className='main-link' to="/product"><p>Перейти на страницу товара</p></Link>
          </div>
      </main>

      <Footer />

    </div>
  );
}

export default HomePage;
