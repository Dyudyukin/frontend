import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./reducers/cart-reducer";
import favoriteReducer from "./reducers/favorite-reducer";

const actionTaken = [];
export const logger = (store) => (next) => (action) => {
  console.log('action', action);
  actionTaken.push(action);
  const nextState = next(action);
  console.log('next state', store.getState());
  console.log('Количество обработанных действий:', actionTaken.length);
  return nextState;
};

export const store = configureStore({
reducer: {
  cart: cartReducer,
  favorite: favoriteReducer,
},

middleware: [logger],
});